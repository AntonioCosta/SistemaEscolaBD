﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEscola
{
	class AlunoBD
	{
		public int inserirAluno(Aluno aluno)
		{
			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("insert into aluno(nome, matricula, data_nascimento, data_matricula) " +
				"values ('"+aluno.getNome()+ "','" + aluno.getMatricula() + "','" + aluno.getDataNasc() + "','" + aluno.getDataMatricula() + "')", conn);

			int result = cmd.ExecuteNonQuery();
			c.desconecta();

			return result;
		}

		public List<Aluno> buscarAlunosPorNome(String nome)
		{
			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("select * from aluno where nome like '%"+ nome +"%'", conn);
			NpgsqlDataReader dRead = cmd.ExecuteReader();
			List<Aluno> alunos = new List<Aluno>();

			while (dRead.Read())
			{
				Aluno a = new Aluno();
				a.setIdAluno(Convert.ToInt32(dRead[0]));
				a.setNome(Convert.ToString(dRead[1]));
				a.setMatricula(Convert.ToString(dRead[2]));
				a.setDataNasc(Convert.ToDateTime(dRead[3]));
				a.setDataMatricula(Convert.ToDateTime(dRead[4]));
				alunos.Add(a);
			}
			c.desconecta();
			return alunos;
		}
	}
}
