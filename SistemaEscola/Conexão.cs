﻿using Npgsql; //foi importado para sair erro
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; //foi importado para sair erro

namespace SistemaEscola
{
	class Conexão
	{
		Npgsql.NpgsqlConnection conn;
		String conexao = "Server=localhost;Port=5432;User Id=postgres;Password=lab;Database=BDSistemaEscola;";


		public NpgsqlConnection conecta()
		{
			//Estabelece ligacoes e bases de dados
			try
			{
				conn = new NpgsqlConnection(conexao);
				conn.Open();
				MessageBox.Show("Conecção Efetuata", "Sucesso");
				return conn;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Erro de ligação");
			}
			return null;
		}

		public void desconecta()
		{
			conn.Close();
		}
	}
}
