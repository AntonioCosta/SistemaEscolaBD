﻿namespace SistemaEscola
{
	partial class FormPrincipal
	{
		/// <summary>
		/// Variável de designer necessária.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpar os recursos que estão sendo usados.
		/// </summary>
		/// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código gerado pelo Windows Form Designer

		/// <summary>
		/// Método necessário para suporte ao Designer - não modifique 
		/// o conteúdo deste método com o editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.novoAlunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buscarAlunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.disciplinaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cadastrarProvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.notaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.registarNotaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.groupBoxAlunos = new System.Windows.Forms.GroupBox();
			this.lblNenhumRegistro = new System.Windows.Forms.Label();
			this.btBuscaAluno = new System.Windows.Forms.Button();
			this.btLimparBusca = new System.Windows.Forms.Button();
			this.gridAluno = new System.Windows.Forms.DataGridView();
			this.colunaNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colunaProva = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colunaNota = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.txtBuscaAluno = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btVoltar = new System.Windows.Forms.Button();
			this.menuStrip1.SuspendLayout();
			this.groupBoxAlunos.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridAluno)).BeginInit();
			this.SuspendLayout();
			// 
			// alunoToolStripMenuItem
			// 
			this.alunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.alunoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoAlunoToolStripMenuItem,
            this.buscarAlunoToolStripMenuItem});
			this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
			this.alunoToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 10, 4, 10);
			this.alunoToolStripMenuItem.Size = new System.Drawing.Size(113, 45);
			this.alunoToolStripMenuItem.Text = "Aluno";
			// 
			// novoAlunoToolStripMenuItem
			// 
			this.novoAlunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.novoAlunoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.novoAlunoToolStripMenuItem.Name = "novoAlunoToolStripMenuItem";
			this.novoAlunoToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
			this.novoAlunoToolStripMenuItem.Text = "Matricular Aluno";
			this.novoAlunoToolStripMenuItem.Click += new System.EventHandler(this.novoAlunoToolStripMenuItem_Click);
			// 
			// buscarAlunoToolStripMenuItem
			// 
			this.buscarAlunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.buscarAlunoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.buscarAlunoToolStripMenuItem.Name = "buscarAlunoToolStripMenuItem";
			this.buscarAlunoToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
			this.buscarAlunoToolStripMenuItem.Text = "Buscar Aluno";
			this.buscarAlunoToolStripMenuItem.Click += new System.EventHandler(this.buscarAlunoToolStripMenuItem_Click);
			// 
			// disciplinaToolStripMenuItem
			// 
			this.disciplinaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.disciplinaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarProvaToolStripMenuItem});
			this.disciplinaToolStripMenuItem.Name = "disciplinaToolStripMenuItem";
			this.disciplinaToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 10, 4, 10);
			this.disciplinaToolStripMenuItem.Size = new System.Drawing.Size(113, 45);
			this.disciplinaToolStripMenuItem.Text = "Prova";
			// 
			// cadastrarProvaToolStripMenuItem
			// 
			this.cadastrarProvaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.cadastrarProvaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.cadastrarProvaToolStripMenuItem.Name = "cadastrarProvaToolStripMenuItem";
			this.cadastrarProvaToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
			this.cadastrarProvaToolStripMenuItem.Text = "Cadastrar Prova";
			this.cadastrarProvaToolStripMenuItem.Click += new System.EventHandler(this.cadastrarProvaToolStripMenuItem_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem,
            this.disciplinaToolStripMenuItem,
            this.notaToolStripMenuItem});
			this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Padding = new System.Windows.Forms.Padding(220, 100, 220, 100);
			this.menuStrip1.Size = new System.Drawing.Size(554, 358);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// notaToolStripMenuItem
			// 
			this.notaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.notaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registarNotaToolStripMenuItem});
			this.notaToolStripMenuItem.Name = "notaToolStripMenuItem";
			this.notaToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 10, 4, 10);
			this.notaToolStripMenuItem.Size = new System.Drawing.Size(113, 45);
			this.notaToolStripMenuItem.Text = "Nota";
			// 
			// registarNotaToolStripMenuItem
			// 
			this.registarNotaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.registarNotaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.registarNotaToolStripMenuItem.Name = "registarNotaToolStripMenuItem";
			this.registarNotaToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
			this.registarNotaToolStripMenuItem.Text = "Registar Nota";
			this.registarNotaToolStripMenuItem.Click += new System.EventHandler(this.registarNotaToolStripMenuItem_Click);
			// 
			// groupBoxAlunos
			// 
			this.groupBoxAlunos.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.groupBoxAlunos.Controls.Add(this.lblNenhumRegistro);
			this.groupBoxAlunos.Controls.Add(this.btBuscaAluno);
			this.groupBoxAlunos.Controls.Add(this.btLimparBusca);
			this.groupBoxAlunos.Controls.Add(this.gridAluno);
			this.groupBoxAlunos.Controls.Add(this.txtBuscaAluno);
			this.groupBoxAlunos.Controls.Add(this.label1);
			this.groupBoxAlunos.Location = new System.Drawing.Point(0, 41);
			this.groupBoxAlunos.Name = "groupBoxAlunos";
			this.groupBoxAlunos.Size = new System.Drawing.Size(554, 317);
			this.groupBoxAlunos.TabIndex = 2;
			this.groupBoxAlunos.TabStop = false;
			this.groupBoxAlunos.Text = "Buscar Alunos";
			this.groupBoxAlunos.Visible = false;
			// 
			// lblNenhumRegistro
			// 
			this.lblNenhumRegistro.AutoSize = true;
			this.lblNenhumRegistro.ForeColor = System.Drawing.Color.Red;
			this.lblNenhumRegistro.Location = new System.Drawing.Point(103, 89);
			this.lblNenhumRegistro.Name = "lblNenhumRegistro";
			this.lblNenhumRegistro.Size = new System.Drawing.Size(144, 13);
			this.lblNenhumRegistro.TabIndex = 5;
			this.lblNenhumRegistro.Text = "Nenhum registro encontrado!";
			this.lblNenhumRegistro.Visible = false;
			// 
			// btBuscaAluno
			// 
			this.btBuscaAluno.Location = new System.Drawing.Point(395, 52);
			this.btBuscaAluno.Name = "btBuscaAluno";
			this.btBuscaAluno.Size = new System.Drawing.Size(75, 23);
			this.btBuscaAluno.TabIndex = 4;
			this.btBuscaAluno.Text = "Buscar";
			this.btBuscaAluno.UseVisualStyleBackColor = true;
			this.btBuscaAluno.Click += new System.EventHandler(this.btBuscaAluno_Click);
			// 
			// btLimparBusca
			// 
			this.btLimparBusca.Location = new System.Drawing.Point(304, 52);
			this.btLimparBusca.Name = "btLimparBusca";
			this.btLimparBusca.Size = new System.Drawing.Size(75, 23);
			this.btLimparBusca.TabIndex = 3;
			this.btLimparBusca.Text = "Limpar";
			this.btLimparBusca.UseVisualStyleBackColor = true;
			this.btLimparBusca.Click += new System.EventHandler(this.btLimparBusca_Click);
			// 
			// gridAluno
			// 
			this.gridAluno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridAluno.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colunaNome,
            this.colunaProva,
            this.colunaNota});
			this.gridAluno.Location = new System.Drawing.Point(6, 113);
			this.gridAluno.Name = "gridAluno";
			this.gridAluno.Size = new System.Drawing.Size(514, 192);
			this.gridAluno.TabIndex = 2;
			this.gridAluno.Visible = false;
			// 
			// colunaNome
			// 
			this.colunaNome.HeaderText = "Nome";
			this.colunaNome.Name = "colunaNome";
			this.colunaNome.Width = 290;
			// 
			// colunaProva
			// 
			this.colunaProva.HeaderText = "Prova";
			this.colunaProva.Name = "colunaProva";
			this.colunaProva.Width = 130;
			// 
			// colunaNota
			// 
			this.colunaNota.HeaderText = "Nota";
			this.colunaNota.Name = "colunaNota";
			this.colunaNota.Width = 50;
			// 
			// txtBuscaAluno
			// 
			this.txtBuscaAluno.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtBuscaAluno.Location = new System.Drawing.Point(103, 26);
			this.txtBuscaAluno.Name = "txtBuscaAluno";
			this.txtBuscaAluno.Size = new System.Drawing.Size(367, 20);
			this.txtBuscaAluno.TabIndex = 1;
			this.txtBuscaAluno.TextChanged += new System.EventHandler(this.txtBuscaAluno_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(59, 29);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Nome:";
			// 
			// btVoltar
			// 
			this.btVoltar.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.btVoltar.ForeColor = System.Drawing.Color.Red;
			this.btVoltar.Location = new System.Drawing.Point(0, 0);
			this.btVoltar.Name = "btVoltar";
			this.btVoltar.Size = new System.Drawing.Size(51, 23);
			this.btVoltar.TabIndex = 3;
			this.btVoltar.Text = "Voltar";
			this.btVoltar.UseVisualStyleBackColor = false;
			this.btVoltar.Click += new System.EventHandler(this.btVoltar_Click);
			// 
			// FormPrincipal
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(554, 358);
			this.Controls.Add(this.menuStrip1);
			this.Controls.Add(this.groupBoxAlunos);
			this.Controls.Add(this.btVoltar);
			this.Name = "FormPrincipal";
			this.Text = "Tela Inicial";
			this.Load += new System.EventHandler(this.FormPrincipal_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.groupBoxAlunos.ResumeLayout(false);
			this.groupBoxAlunos.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridAluno)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem novoAlunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem buscarAlunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem disciplinaToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem cadastrarProvaToolStripMenuItem;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem notaToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem registarNotaToolStripMenuItem;
		private System.Windows.Forms.GroupBox groupBoxAlunos;
		private System.Windows.Forms.Label lblNenhumRegistro;
		private System.Windows.Forms.Button btBuscaAluno;
		private System.Windows.Forms.Button btLimparBusca;
		private System.Windows.Forms.DataGridView gridAluno;
		private System.Windows.Forms.TextBox txtBuscaAluno;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btVoltar;
		private System.Windows.Forms.DataGridViewTextBoxColumn colunaNome;
		private System.Windows.Forms.DataGridViewTextBoxColumn colunaProva;
		private System.Windows.Forms.DataGridViewTextBoxColumn colunaNota;
	}
}

