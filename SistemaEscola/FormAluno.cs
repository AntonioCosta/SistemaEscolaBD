﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormAluno : Form
	{
		//List<Aluno> alunos;
		
		public FormAluno(List<Aluno> alunos)
		{
			InitializeComponent();
			//this.alunos = alunos;
		}

		private void txtNome_TextChanged(object sender, EventArgs e)
		{

		}

		private void btSalvarAluno_Click(object sender, EventArgs e)
		{
			Aluno a = new Aluno();

			a.setNome(txtNome.Text);
			a.setDataNasc(txtDataNasc.Value);
			a.setMatricula(txtMatricula.Text);
			a.setDataMatricula(txtDataMatricula.Value);

			//alunos.Add(a);

			AlunoBD alunoBD = new AlunoBD();
			int result = alunoBD.inserirAluno(a);
			if (result == 0)
			{
				MessageBox.Show("Erro ao cadastrar aluno");
			}
			else
			{
				MessageBox.Show("Aluno salvo com sucesso", "Sucesso", MessageBoxButtons.OK);
				limparCampos();
			}
		}
		public void limparCampos()
		{
			txtNome.Text = "";
			txtDataNasc.Text = "";
			txtMatricula.Text = "";
			txtDataMatricula.Text = "";
		}
	}
}
