﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormBuscaAluno : Form
	{
		List<Aluno> alunos;
		//List<Prova> provas;
		//List<Nota> notas;
		Aluno novoAluno;
		FormNota novaNota;

		public FormBuscaAluno(Aluno novoAluno, List<Aluno> alunos, FormNota formNota)
		{
			InitializeComponent();
			this.alunos = alunos;
			novaNota = formNota;
			this.novoAluno = novoAluno;
			gridResultadoAluno.ReadOnly = true;
			
		}

		private void FormBuscaAluno_Load(object sender, EventArgs e)
		{

		}

		private void txtBuscarAluno_TextChanged(object sender, EventArgs e)
		{
			gridResultadoAluno.Rows.Clear();
			AlunoBD alunoBD = new AlunoBD();
			List<Aluno> alunoBusca = alunoBD.buscarAlunosPorNome(txtBuscarAluno.Text);

			gridResultadoAluno.Visible = true;

			/*foreach (Aluno a in alunos)
			{
				if (a.getNome().Contains(txtBuscarAluno.Text))
				{
					alunoBusca.Add(a);
				}
			}*/

			
			foreach (Aluno a in alunoBusca)
			{

				gridResultadoAluno.Rows.Add(a.getIdAluno(), a.getNome(), a.getMatricula(), a.getDataNasc(), a.getDataMatricula());
			}
		}

		private void gridResultadoAluno_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			novaNota.txtAluno.Text = gridResultadoAluno.CurrentRow.Cells[1].Value.ToString();
			novoAluno.setNome(novaNota.txtAluno.Text);

			novaNota.lblidAluno.Text = gridResultadoAluno.CurrentRow.Cells[0].Value.ToString();
			novoAluno.setIdAluno(Convert.ToInt16(novaNota.lblidAluno.Text));
			this.Close();
		}
	}
}
