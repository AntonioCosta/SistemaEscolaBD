﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormBuscaProva : Form
	{
		//List<Aluno> alunos;
		List<Prova> provas;
		Prova novaProva;
		FormNota novaNota;

		public FormBuscaProva(Prova novaProva, List<Prova> provas, FormNota formNota)
		{
			InitializeComponent();
			this.provas = provas;
			novaNota = formNota;
			this.novaProva = novaProva;
			gridResultadoProva.ReadOnly = true;

		}

		private void txtBuscarProva_TextChanged(object sender, EventArgs e)
		{
			gridResultadoProva.Rows.Clear();
			ProvaBD provaBD = new ProvaBD();
			List<Prova> provaBusca = provaBD.buscarProvasPorDescricao(txtBuscarProva.Text);

			gridResultadoProva.Visible = true;

			/*foreach (Prova p in provas)
			{
				if (p.getDescricao().Contains(txtBuscarProva.Text))
				{
					provaBusca.Add(p);
				}
			}*/


			foreach (Prova p in provaBusca)
			{

				gridResultadoProva.Rows.Add(p.getIdProva(), p.getDescricao(), p.getDataProva());
			}
		}

		private void gridResultadoProva_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			novaNota.txtProva.Text = gridResultadoProva.CurrentRow.Cells[1].Value.ToString();
			novaProva.setDescricao(novaNota.txtProva.Text);

			novaNota.lblidProva.Text = gridResultadoProva.CurrentRow.Cells[0].Value.ToString();
			novaProva.setIdProva(Convert.ToInt16(novaNota.lblidProva.Text));

			this.Close();
		}
	}
}
