﻿namespace SistemaEscola
{
	partial class FormNota
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtProva = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtAluno = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btBuscarAluno = new System.Windows.Forms.Button();
			this.btBuscarProva = new System.Windows.Forms.Button();
			this.txtNota = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.btSalvarNota = new System.Windows.Forms.Button();
			this.lblidAluno = new System.Windows.Forms.Label();
			this.lblidProva = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// txtProva
			// 
			this.txtProva.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtProva.Enabled = false;
			this.txtProva.Location = new System.Drawing.Point(49, 85);
			this.txtProva.Name = "txtProva";
			this.txtProva.Size = new System.Drawing.Size(172, 20);
			this.txtProva.TabIndex = 7;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(15, 88);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(38, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Prova:";
			// 
			// txtAluno
			// 
			this.txtAluno.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtAluno.Enabled = false;
			this.txtAluno.Location = new System.Drawing.Point(49, 46);
			this.txtAluno.Name = "txtAluno";
			this.txtAluno.Size = new System.Drawing.Size(230, 20);
			this.txtAluno.TabIndex = 5;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(15, 49);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(37, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Aluno:";
			// 
			// btBuscarAluno
			// 
			this.btBuscarAluno.Location = new System.Drawing.Point(285, 44);
			this.btBuscarAluno.Name = "btBuscarAluno";
			this.btBuscarAluno.Size = new System.Drawing.Size(49, 23);
			this.btBuscarAluno.TabIndex = 8;
			this.btBuscarAluno.Text = "Buscar";
			this.btBuscarAluno.UseVisualStyleBackColor = true;
			this.btBuscarAluno.Click += new System.EventHandler(this.btBuscarAluno_Click);
			// 
			// btBuscarProva
			// 
			this.btBuscarProva.Location = new System.Drawing.Point(227, 83);
			this.btBuscarProva.Name = "btBuscarProva";
			this.btBuscarProva.Size = new System.Drawing.Size(49, 23);
			this.btBuscarProva.TabIndex = 9;
			this.btBuscarProva.Text = "Buscar";
			this.btBuscarProva.UseVisualStyleBackColor = true;
			this.btBuscarProva.Click += new System.EventHandler(this.btBuscarProva_Click);
			// 
			// txtNota
			// 
			this.txtNota.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtNota.Location = new System.Drawing.Point(150, 134);
			this.txtNota.Name = "txtNota";
			this.txtNota.Size = new System.Drawing.Size(71, 20);
			this.txtNota.TabIndex = 11;
			this.txtNota.TextChanged += new System.EventHandler(this.txtNota_TextChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(116, 137);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(33, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "Nota:";
			// 
			// btSalvarNota
			// 
			this.btSalvarNota.Location = new System.Drawing.Point(150, 221);
			this.btSalvarNota.Name = "btSalvarNota";
			this.btSalvarNota.Size = new System.Drawing.Size(49, 23);
			this.btSalvarNota.TabIndex = 12;
			this.btSalvarNota.Text = "Salvar";
			this.btSalvarNota.UseVisualStyleBackColor = true;
			this.btSalvarNota.Click += new System.EventHandler(this.btSalvarNota_Click);
			// 
			// lblidAluno
			// 
			this.lblidAluno.AutoSize = true;
			this.lblidAluno.Location = new System.Drawing.Point(49, 66);
			this.lblidAluno.Name = "lblidAluno";
			this.lblidAluno.Size = new System.Drawing.Size(35, 13);
			this.lblidAluno.TabIndex = 13;
			this.lblidAluno.Text = "label4";
			// 
			// lblidProva
			// 
			this.lblidProva.AutoSize = true;
			this.lblidProva.Location = new System.Drawing.Point(49, 108);
			this.lblidProva.Name = "lblidProva";
			this.lblidProva.Size = new System.Drawing.Size(35, 13);
			this.lblidProva.TabIndex = 14;
			this.lblidProva.Text = "label5";
			// 
			// FormNota
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(347, 302);
			this.Controls.Add(this.lblidProva);
			this.Controls.Add(this.lblidAluno);
			this.Controls.Add(this.btSalvarNota);
			this.Controls.Add(this.txtNota);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.btBuscarProva);
			this.Controls.Add(this.btBuscarAluno);
			this.Controls.Add(this.txtProva);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtAluno);
			this.Controls.Add(this.label1);
			this.Name = "FormNota";
			this.Text = "FormNota";
			this.Load += new System.EventHandler(this.FormNota_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		public System.Windows.Forms.TextBox txtProva;
		private System.Windows.Forms.Label label2;
		public System.Windows.Forms.TextBox txtAluno;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btBuscarAluno;
		private System.Windows.Forms.Button btBuscarProva;
		private System.Windows.Forms.TextBox txtNota;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btSalvarNota;
		public System.Windows.Forms.Label lblidAluno;
		public System.Windows.Forms.Label lblidProva;
	}
}