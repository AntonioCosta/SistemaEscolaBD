﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormNota : Form
	{
		Aluno novoAluno;
		Prova novaProva;
		//Nota novaNota;
		List<Aluno> alunos;
		List<Prova> provas;
		//List<Nota> notas;

		public FormNota(List<Aluno> alunos, List<Prova> provas, List<Nota> notas)
		{
			InitializeComponent();
			
			this.alunos = alunos;
			this.provas = provas;
			//this.notas = notas;
			novoAluno = new Aluno();
			novaProva = new Prova();
			//novaNota = new Nota();
		}

		private void FormNota_Load(object sender, EventArgs e)
		{

		}

		private void btBuscarAluno_Click(object sender, EventArgs e)
		{
			FormBuscaAluno busca = new FormBuscaAluno(novoAluno, alunos, this);
			busca.Show();
		}

		private void btBuscarProva_Click(object sender, EventArgs e)
		{
			FormBuscaProva busca = new FormBuscaProva(novaProva, provas, this);
			busca.Show();
		}

		private void btSalvarNota_Click(object sender, EventArgs e)
		{
			Nota nota = new Nota();

			//nota.setAluno(novoAluno);
			//nota.setProva(novaProva);
			//notas.Add(nota);

			//MessageBox.Show("Nota registrada com sucesso", "Sucesso", MessageBoxButtons.OK);
			//limparCampos();

			double n = Convert.ToDouble(txtNota.Text);
			if (n < 0 || n >10)
			{
				MessageBox.Show("Nota Inválida", "Erro", MessageBoxButtons.OK);
				txtNota.Text = "";
			}
			else
			{
				nota.setAluno(novoAluno);
				nota.setProva(novaProva);
				nota.setNota(n);
				
				//notas.Add(nota);

				NotaBD notaBD = new NotaBD();
				int result = notaBD.inserirNota(nota, novoAluno, novaProva);
				if (result == 0)
				{
					MessageBox.Show("Erro ao registrar nota");
				}
				else
				{
					MessageBox.Show("Nota registrada com sucesso", "Sucesso", MessageBoxButtons.OK);
					limparCampos();
				}
			}
		}
		public void limparCampos()
		{
			txtAluno.Text = "";
			txtProva.Text = "";
			txtNota.Text = "";
		}

		private void txtNota_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
