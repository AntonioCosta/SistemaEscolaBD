﻿namespace SistemaEscola
{
	partial class FormProva
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btSalvarProva = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.txtDescricao = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtDataProva = new System.Windows.Forms.DateTimePicker();
			this.SuspendLayout();
			// 
			// btSalvarProva
			// 
			this.btSalvarProva.Location = new System.Drawing.Point(147, 215);
			this.btSalvarProva.Name = "btSalvarProva";
			this.btSalvarProva.Size = new System.Drawing.Size(75, 23);
			this.btSalvarProva.TabIndex = 17;
			this.btSalvarProva.Text = "Salvar";
			this.btSalvarProva.UseVisualStyleBackColor = true;
			this.btSalvarProva.Click += new System.EventHandler(this.button1_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(47, 100);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(104, 13);
			this.label3.TabIndex = 13;
			this.label3.Text = "Data de Realização:";
			// 
			// txtDescricao
			// 
			this.txtDescricao.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtDescricao.Location = new System.Drawing.Point(147, 44);
			this.txtDescricao.Name = "txtDescricao";
			this.txtDescricao.Size = new System.Drawing.Size(173, 20);
			this.txtDescricao.TabIndex = 10;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(93, 47);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(58, 13);
			this.label1.TabIndex = 9;
			this.label1.Text = "Descrição:";
			// 
			// txtDataProva
			// 
			this.txtDataProva.Location = new System.Drawing.Point(147, 94);
			this.txtDataProva.Name = "txtDataProva";
			this.txtDataProva.Size = new System.Drawing.Size(173, 20);
			this.txtDataProva.TabIndex = 18;
			// 
			// FormProva
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(377, 327);
			this.Controls.Add(this.txtDataProva);
			this.Controls.Add(this.btSalvarProva);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtDescricao);
			this.Controls.Add(this.label1);
			this.Name = "FormProva";
			this.Text = "FormProva";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btSalvarProva;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtDescricao;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker txtDataProva;
	}
}