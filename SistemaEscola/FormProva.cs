﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormProva : Form
	{
		//List<Prova> provas;

		public FormProva(List<Prova> provas)
		{
			InitializeComponent();
			//this.provas = provas;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Prova p = new Prova();

			p.setDescricao(txtDescricao.Text);
			p.setDataProva(txtDataProva.Value);

			//provas.Add(p);

			ProvaBD provaBD = new ProvaBD();
			int result = provaBD.inserirProva(p);
			if (result == 0)
			{
				MessageBox.Show("Erro ao cadastrar prova");
			}
			else
			{
				MessageBox.Show("Prova registrada com sucesso", "Sucesso", MessageBoxButtons.OK);
				limparCampos();
			}
		}
		public void limparCampos()
		{
			txtDescricao.Text = "";
			txtDataProva.Text = "";
		}
	}
}
