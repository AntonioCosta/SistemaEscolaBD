﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEscola
{
	public class Nota
	{
		private Aluno aluno;
		private Prova prova;
		private double nota;

		//aluno
		public void setAluno(Aluno aluno)
		{
			this.aluno = aluno;
		}
		public Aluno getAluno()
		{
			return aluno;
		}
		//prova
		public void setProva(Prova prova)
		{
			this.prova = prova;
		}
		public Prova getProva()
		{
			return prova;
		}
		//nota
		public void setNota(double nota)
		{
			this.nota = nota;
		}
		public double getNota()
		{
			return nota;
		}
	}
}
