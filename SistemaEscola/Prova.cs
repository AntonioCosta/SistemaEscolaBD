﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEscola
{
	public class Prova
	{
		private int idProva;
		private DateTime dataProva;
		private String descricao;

		//idProva
		public void setIdProva(int idProva)
		{
			this.idProva = idProva;
		}
		public int getIdProva()
		{
			return idProva;
		}
		//dataProva
		public void setDataProva(DateTime dataProva)
		{
			this.dataProva = dataProva;
		}
		public DateTime getDataProva()
		{
			return dataProva;
		}
		//descricao
		public void setDescricao(String descricao)
		{
			this.descricao = descricao;
		}
		public String getDescricao()
		{
			return descricao;
		}
	}
}
