﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEscola
{
	class ProvaBD
	{
		public int inserirProva(Prova prova)
		{
			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("insert into prova(data_realizacao, descricao) " +
				"values ('" + prova.getDataProva() + "','" + prova.getDescricao() + "')", conn);

			int result = cmd.ExecuteNonQuery();
			c.desconecta();

			return result;
		}

		public List<Prova> buscarProvasPorDescricao(String descricao)
		{
			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("select * from prova where descricao like '%" + descricao + "%'", conn);
			NpgsqlDataReader dRead = cmd.ExecuteReader();
			List<Prova> provas = new List<Prova>();

			while (dRead.Read())
			{
				Prova p = new Prova();
				p.setIdProva(Convert.ToInt32(dRead[0]));
				p.setDataProva(Convert.ToDateTime(dRead[1]));
				p.setDescricao(Convert.ToString(dRead[2]));
				provas.Add(p);
			}
			c.desconecta();
			return provas;
		}
	}
}
